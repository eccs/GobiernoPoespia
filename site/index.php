<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
<title>Gobierno Poespía: Recolección de audios</title>
<link rel="stylesheet" type="text/css" href="theme.css">
</head>
<body>



<?php
$target_dir = "get/";
$extfile = $target_dir."extensions.txt";

$uploadOk = 1;

//$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$imageFileType = pathinfo(basename($_FILES["fileToUpload"]["name"]),PATHINFO_EXTENSION);


$json = file_get_contents("https://raw.githubusercontent.com/Escenaconsejo/GobiernoEspiaJSON/master/GobiernoEspia.json");
$decoded = json_decode($json);

if(isset($_GET["id"])){
	$index = $_GET["id"]-1;
}
else{
	$index = rand(0,count($decoded)-1);
}
$sms = $decoded[$index];
$id = $sms->{'id'};
$date = date('c');
$newfilename = sprintf("%sfile-%03d-%s.%s",$target_dir,$id,$date,$imageFileType);
?>

<div id="content">
<h1>Gobierno Poespía<br/> Recolección de voces</h1>
<h3>Comparte esta dirección: <a href="https://eccs.world/poespia">eccs.world/poespia</a></h3>




<?php
//$nfilename = sprintf("file-%08d.%s",$newnum,$imageFileType);
//$newfilename = sprintf("%s%s",$target_dir,$nfilename);
$target_file = $newfilename;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
	// Check if file already exists
	if (file_exists($target_file)) {
	    echo "Sorry, file already exists.";
	    $uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 25000000) {
	    echo "Error: el archivo es muy grande (tamaño áximo): 20MB)";
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "mp3" && $imageFileType != "aac" && $imageFileType != "m4a"
	&& $imageFileType != "wav" && $imageFileType!="ogg" && $imageFileType!="mp4") {
	    echo "Error, asegúrate que el formato es mp3, aac, m4a, wav, u ogg";
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		    file_put_contents($numfile,$newnum);

		    file_put_contents($extfile,$nfilename,FILE_APPEND);

?>
<p>
	El archivo <?php echo basename( $_FILES["fileToUpload"]["name"]); ?> se ha guardado exitosamente con otro nombre. ¡Gracias!
</p>

<a href='index.php'>Regresa</a>
<?php
	    } else {
	        echo "Sorry, there was an error uploading your file.";
	    }
	}

}
else{
?>

<!-- The instructions should go here -->

<ul>
<li><a href="#demo">Demo</a></li>
<li><a href="#gobiernoespia">#GobiernoEspía</a></li>
<li><a href="#proyecto">El proyecto</a></li>
<li><a href="#contribuye">Contribuye</a></li>
<li><a href="#comparte">Comparte</a></li>
<li><a href="#referencias">Referencias</a></li>
</ul>
<a name="demo"></a>
<h2>Demo</h2>
<video width=600 height=320 controls>
<source src="Demo.mp4" type="video/mp4">
<i>Tu navegador no soporta la reproducción de este video</i>
</video>

<a name="gobiernoespia"></a>
<h2>#GobiernoEspía</h2>
<p>
En junio de 2017 se dieron a conocer varios informes <i>"que documentan el uso ilegal de Pegasus (un software sofisticado de vigilancia desarrollado por la firma israelí NSO Group y comercializado solamente a gobiernos) en contra de periodistas, defensores y defensoras de derechos humanos así como a activistas que denuncian la corrupción en México."</i> (<a href="https://r3d.mx/2017/07/28/gobiernoespia-40-dias-despues/">#GobiernoEspía: 40 días después, R3D: Red en Defensa de los Derechos Digitales</a>).
</p>
<p>
El uso de dicho software implica enviar <i>"mensajes de texto con enlaces asociados a la infraestructura de Pegasus, que al hacer clic en ellos, permiten la descarga de un programa que infecta el dispositivo, permitiendo al atacante tener acceso irrestricto a archivos, documentos, contactos, correos, conversaciones y funciones del aparato."</i> (ibid.)
</p>

<a name="proyecto"></a>
<h2>El proyecto</h2>
<p>
Hoy estamos recolectando voces mexicanas para la realización de una pieza artística colaborativa y multimedia en denuncia al hecho.
</p>
<p>
La idea es juntar diferentes voces leyendo alguno de los mensajes documentados.
Estas grabaciones se usarán en una composición de audio generativo
que podrá ser escuchada acompañada de los textos correspondientes.
</p>
<p>
Esta pieza se estrenará en el festival <a href="http://newlatinwave.com/">New Latin Wave</a> 2017,
el 22 de octubre en Brooklyn, NY.
Además, tan pronto esté lista podrá ser también consultada en línea.
</p>

<a name="contribuye"></a>
<h2>Contribuye</h2>
<p id="instrucciones">
Grábate leyendo el siguiente mensaje y sube el archivo de audio (formato aac, m4a, mp3, ogg, wa, mp4)
usando los botones correspondientes debajo. 
O si lo prefieres y conoces a alguien de Escenaconsejo, mándaselo por mail o por mensaje en Signal o WhatsApp:
</p>

<div id="mensaje">
<?php
echo $sms->{'mensaje'};
?>
</div>

<div id="descripcion">
Este mensaje fue enviado el <?php echo $sms->{'fecha'};?> a <?php echo $sms->{'objetivo'};?> y está registrado en el caso <?php echo $sms->{'caso'};?> (id: <a href="?id=<?php echo $sms->{'id'};?>"><?php echo $sms->{'id'};?></a>).
</div>

<form action="" method="post" enctype="multipart/form-data">
<br/>
	        <input type="file" name="fileToUpload" id="fileToUpload"><br/>
		    <input type="submit" value="Enviar" name="submit">
	    </form>

<p>
Para obtener otro mensaje, <a href="https://eccs.world/poespia">vuelve a cargar la página</a>.
</p>

<a name="comparte"></a>
<h2>Comparte</h2>
<p>Necesitamos muchas voces mexicanas, comparte esta dirección: <a href="https://eccs.world/poespia">eccs.world/poespia</a><p>

<div id="referencias">
<a name="referencias"></a>
<h2>Referencias</h2>
<ul>
<li><a href="https://r3d.mx/gobiernoespia">Gobierno Espía: Vigilancia sistemática a periodistas y defensores de derechos humanos en México</a> (Junio 2017) - <a href="https://articulo19.org">Article19</a>, <a href="https://r3d.mx">R3D - Red en Defensa de los Derechos Digitales</a> y <a href="https://socialtic.org/">SocialTIC</a></li>
<li><a href="https://r3d.mx/2017/07/10/giei-gobierno-espia/">Confirman que el GIEI también fue objetivo del #GobiernoEspía</a> (10 de julio, 2017) R3D</li>
<li><a href="https://r3d.mx/2017/07/28/gobiernoespia-40-dias-despues/">#GobiernoEspía: 40 días después</a> (28 de julio, 2017) R3D</li>
<li><a href="https://r3d.mx/2017/08/02/gobierno-espia-caso-narvarte/">#GobiernoEspía: Representantes de víctimas del caso Narvarte fueron objetivo de Pegasus</a> (2 de agosto, 2017) R3D</li>

</div>

<div id="footer">
<a href="https://github.com/Escenaconsejo/GobiernoEspiaJSON">Archivo de mensajes</a> 
<a href="https://github.com/Escenaconsejo/GobiernoPoespia">Código fuente del sitio y proyecto</a>
<br/>
Proyecto de <a href="https://escenaconsejo.org">Escenaconsejo</a>
</div>

</div>

	    </body>
	    </html>
<?php
}
?>
