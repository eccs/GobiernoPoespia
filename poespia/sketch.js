var sms = [];
var numArchivos;
var sonidos = [];
var finishedLoadingAudio = false;
var finishedLoadingSMS = false;

var playlistPtr;
var playlist = [];
var indiceActual;
var mensajeActual;

var font;
var fontFirma;

function preload(){
	// Font de https://www.fontsquirrel.com/fonts/Kingthings-Foundation
	font = loadFont('data/Kingthings_Foundation.ttf');
	// Font de https://www.fontsquirrel.com/fonts/Freebooter-Script
	fontFirma = loadFont('data/freebooterscript.ttf');

	soundFormats('m4a','ogg','mp3');
	loadJSON('data/GobiernoEspia.json',mensajesCargados);

	loadJSON('data/files.json',archivosCargados);
}

function windowResized(){
	resizeCanvas(windowWidth*0.95,windowHeight*0.95);
}

function setup() {
	createCanvas(windowWidth*0.95,windowHeight*0.95);
	// Colores de http://www.colourlovers.com/palette/1193/dissolved_old_paper
	fill(68,68,66);

}

function draw() {
	background(238,233,220);

	if(finishedLoadingAudio && finishedLoadingSMS){
		update();
		if(mensajeActual!=null){
		// Destinatario
		textFont(fontFirma);
		textSize(50);
		text("Para: "+mensajeActual.objetivo, width*0.1, height*0.2);

		// Fecha
		textSize(30);
		text(mensajeActual.fecha,width*0.75,height*0.2);

		// Mensaje
		textFont(font);
		textSize(42);
		text(mensajeActual.mensaje,width*0.15,height*0.35,width*0.7,height);
		}
	}

	// Firma
	textFont(fontFirma);
	textSize(80);
	var firma = "Gobierno Poespia";
	var w = textWidth(firma);
	text(firma, width*0.55, height*0.90);

	// URL
	textFont(font);
	textSize(20);
	var url = "eccs.world/poespia";
	text(url,width*0.1, height*0.90);
}

function archivosCargados(archivos){
	numArchivos = archivos.length;
	var archivo;
	for(var i=0; i< archivos.length;i++){
		archivo = archivos[i];
		var id = ('000'+archivo.id).slice(-3);
		var filename = 'data/file-'+id+'.'+archivo.formato;
		sonidos.push({'id': archivo.id, 'filename': filename});
		console.log('Cargando '+filename+'...');
		loadSound(filename,cargaSonido);
	}
}

function cargaSonido(sonido){
	var full = true;
	for(var i=0; i<sonidos.length; i++){
		if(sonidos[i].filename == sonido.file){
			sonidos[i].sonido = sonido;
		}

		if(sonidos[i].sonido == undefined){
			full = false;
		}

	}
	if(full){
		console.log("Terminado de cargar");
		console.log(sonidos);

		finishedLoadingAudio = true;

		for(var i=0; i<sonidos.length;i++){
			playlist[i] = i;
		}
		restartPlaylist();
		playNext();
	}
}

function restartPlaylist(){
	console.log("Nueva playlist");
	shuffle(playlist,true);
	console.log(playlist);
	playlistPtr = 0;
}


function playNext(){
	if(playlistPtr>=sonidos.length){
		restartPlaylist();
	}

	indiceActual = playlist[playlistPtr];
	sonidos[indiceActual].sonido.play();
	console.log("Inicia indice: "+indiceActual+', ID:'+sonidos[indiceActual].id);

	playlistPtr++;


	mensajeActual = getSMSconId(sonidos[indiceActual].id);
	
}

function update(){
	// If sound is not playing, i.e. it stopped
	var s = sonidos[indiceActual].sonido;
//	console.log(s.duration() - s.currentTime());
	if(!s.isPlaying()){
		playNext();
	}
	
}

function mensajesCargados(m){
	sms = m;
	finishedLoadingSMS = true;
}

function getSMSconId(id){
	var m;
	for(var i=0; i<sms.length; i++){
		m = sms[i];
		if(m.id == id){
			return m;
		}
	}
	return null;
}
